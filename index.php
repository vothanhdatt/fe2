<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FE2</title>
    <link rel="stylesheet" type="text/css" href="./public/css/styles.css">
    <link rel="icon" href="./public/images/logo.svg">
</head>

<body>

    <div class="left">
        <div class="score">
            Score: 0
        </div>
        <div class="menu">
            <div class="new-game">
                <p>New game</p>
            </div>
            <div class="pause-resume">
                <button class="pause">Pause</button>
                <button class="resume" style="display: none;">Resume</button>
            </div>
            <div class="speak">
                <img class="turnOn" src="./public/images/loa.svg">
                <img class="turnOff" src="./public/images/tatLoa.svg" style="display: none;">
            </div>
            <div class="default-rankings ranking-on">
                <img src="./public/images/ranking-on.png">

            </div>
            <div class="default-rankings ranking-off">
                <img src="./public/images/ranking-off.png">

            </div>
        </div>
        <div class="key">
            <input type="hidden" name="" value="">

        </div>
        <div class="arrow-area">
            <tr>
                <td class="arrow">
                    <img class="arrow-img" src="./public/images/arrow.png" alt="">

                </td>
                <td class="arrow">
                    <img class="arrow-img" src="./public/images/arrow.png" alt="">

                </td>
                <td class="arrow">
                    <img class="arrow-img" src="./public/images/arrow.png" alt="">

                </td>
                <td class="arrow">
                    <img class="arrow-img" src="./public/images/arrow.png" alt="">

                </td>

            </tr>
        </div>

    </div>
    <div class="content">

    </div>
    <div class="over">


    </div>



    <div class="form">
        <h1>Nhập tên người chơi</h1>
        <input class="username" type="text" placeholder="Ex: name"> <br>
        <button class="abc" type="submit">Submit</button>
    </div>

    <div class="rank">
        <h1>Bảng xếp hạng</h1>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Hạng</th>
                    <th scope="col">Tên</th>
                    <th scope="col">Điểm</th>

                </tr>
            </thead>
            <tbody class="tbody">



            </tbody>
        </table>
    </div>
    <audio class="audio">
    </audio>
    <audio class="game-over">
        <source src="./public/audio/game_over.mp3" type="audio/mpeg">
    </audio>

    <audio class="music-bg">
        <source src="./public/audio/music_bg.mp3" type="audio/mpeg">
    </audio>
    <audio class="boom-audio">
        <source src="./public/audio/boom.mp3" type="audio/mpeg">
    </audio>
    <audio class="high-score">
        <source src="./public/audio/high-score.mp3" type="audio/mpeg">
    </audio>
    <script src="./public/js/index.js"></script>

</body>

</html>