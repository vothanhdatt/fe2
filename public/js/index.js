if (!localStorage.getItem("id")) {
  $insertUser = document.querySelector(".abc");
  $form = document.querySelector(".form");
  $insertUser.addEventListener("click", function () {
    $username = document.querySelector(".username");
    localStorage.setItem("name", $username.value);
    localStorage.setItem("score", 0);
    $form.style.display = "none";
    //localStorage.setItem("id", $username.value);
    insertScore();
  });
} else {
  $form = document.querySelector(".form");
  $form.style.display = "none";
}
//Mảng bảng chữ jav
var jav = [
  "あ",
  "い",
  "う",
  "え",
  "お",
  "か",
  "き",
  "く",
  "け",
  "こ",
  "さ",
  "し",
  "す",
  "せ",
  "そ",
  "た",
  "ち",
  "つ",
  "て",
  "と",
  "な",
  "に",
  "ぬ",
  "ね",
  "の",
  "は",
  "ひ",
  "ふ",
  "へ",
  "ほ",
  "ま",
  "み",
  "む",
  "め",
  "も",
  "や",
  "ゆ",
  "よ",
  "ら",
  "り",
  "る",
  "れ",
  "ろ",
  "わ",
  "を",
  "ん",
  "零",
  "一",
  "二",
  "三",
  "四",
  "五",
  "六",
  "七",
  "八",
  "九",
  "十",
];
//Mảng chứ latinh
var vn = [
  "a",
  "i",
  "u",
  "e",
  "o",
  "ka",
  "ki",
  "ku",
  "ke",
  "ko",
  "sa",
  "chi",
  "su",
  "se",
  "so",
  "ta",
  "chi",
  "tsu",
  "te",
  "to",
  "na",
  "ni",
  "nu",
  "ne",
  "no",
  "ha",
  "hi",
  "fu",
  "he",
  "ho",
  "ma",
  "mi",
  "mu",
  "me",
  "mo",
  "ya",
  "yu",
  "yo",
  "ra",
  "ri",
  "ru",
  "re",
  "ro",
  "wa",
  "wo",
  "n",
  "rei",
  "ichi",
  "ni",
  "san",
  "yon",
  "go",
  "roku",
  "nana",
  "hachi",
  "kyuu",
  "juu",
];

//Mảng các màu quả pong póng
var color = [
  "blue",
  "green",
  "yellow",
  "pink",
  "gray",
  "skyblue",
  "slateblue",
  "coral",
  "cyan",
];

//Khai báo biến setInterval
let gameAninmation;
let generateBall;

//Biến element
let btnNewGame = document.querySelector(".new-game");
let btnPauseResume = document.querySelector(".pause-resume");
let btnPause = document.querySelector(".pause");
let btnResume = document.querySelector(".resume");
let btnOn = document.querySelector(".turnOn");
let btnOff = document.querySelector(".turnOff");
let content = document.querySelector(".content");
let over = document.querySelector(".over");

//Vị trí ban đầu của quả pong póng
let ballStartY = 0;

//Điểm
let score = 0;

//Trạng thái của game
gameStatus = "LOADING";

gameMusic = "ON";

//Ký tự nhập vào từ bàn phím
let string = "";

//Thời gian tạo ra pong póng
let timeGenerateBall = 2000;

//Thời gian rơi của pong póng

let timeBallAnimation = 100;

//Hàm tạo ra 1 quả pong póng
function createBall(index, left, color) {
  let hiragana = jav[index];
  let latinh = vn[index];
  let boom_start = random(10);
  if (boom_start == 5) {
    var html = `<div class="ball ${latinh} boom"  style="left:${left}px;  ">
    <div class="text">
        <div class="jav" > ${hiragana}
        </div>
        <div class="vn">${latinh}</div>
        
    </div>
    </div> `;
    content.innerHTML += html;
  } else if (boom_start == 6) {
    var html = `<div class="ball ${latinh} start"  style="left:${left}px; background-color:${color}">
    <div class="icon-start">⭐ </div>
    <div class="text">
        <div class="jav" > ${hiragana} 
        </div>
        <div class="vn">${latinh}</div>
        
    </div>
    </div> `;
    content.innerHTML += html;
  } else {
    var html = `<div class="ball ${latinh}"  style="left:${left}px; background-color:${color}">
    <div class="text">
        <div class="jav" > ${hiragana} 
        </div>
        <div class="vn">${latinh}</div>
        
    </div>
    </div> `;
    content.innerHTML += html;
  }
}

//Hàm chuyển động rơi của pong póng
const ballAnimation = () => {
  let ball = document.querySelectorAll(".ball");
  ball.forEach((ball) => {
    let ballPosition = ball.getBoundingClientRect();
    let y = ballPosition.top + 2;
    ball.style.transform = `translateY(${y}px)`;
  });
};

//Hàm chạy game
const runGame = () => {
  //over.style.display = "block";
  if (gameStatus == "RUNNING") {
    let ball = document.querySelectorAll(".ball");
    clearInterval(gameAninmation);
    clearInterval(generateBall);
    clearInterval(checkGameOver);
    playing = false;
    if (ball) {
      ball.forEach((ball) => {
        ball.remove();
      });
    }
  }

  if (gameStatus != "RESUME") {
    if (gameMusic == "ON") {
      loadAudio(".music-bg", true, 0.2, null);
    }

    score = 0;
    let scoreElement = document.querySelector(".score");
    let arrowArea = document.querySelector(".arrow-area");

    scoreElement.innerHTML = `Score: ${score}`;
    arrowArea.innerHTML = `
    <tr>
      <td class="arrow">
          <img class="arrow-img" src="./public/images/arrow.png" alt="">

      </td>
      <td class="arrow">
          <img class="arrow-img" src="./public/images/arrow.png" alt="">

      </td>
      <td class="arrow">
          <img class="arrow-img" src="./public/images/arrow.png" alt="">

      </td>
      <td class="arrow">
          <img class="arrow-img" src="./public/images/arrow.png" alt="">

      </td>

    </tr>`;
  }
  gameStatus = "RUNNING";

  //Bong bóng rơi nhanh dần theo thời gian
  fall();
  //Tạo ra bong bóng nhanh dần theo thời gian
  createMultiBall();
};

//Hàm bong bóng rơi nhanh dần theo thời gian
function fall() {
  gameAninmation = setTimeout(() => {
    ballAnimation();
    fall();
    if (checkGameOver() == true) {
      gameStatus = "GAME_OVER";
      timeBallAnimation = 100;
      timeGenerateBall = 2000;
      loadAudio(".music-bg", true, 0);
      if (gameMusic == "ON") {
        loadAudio(".game-over", true, 0.5, null);
      }

      clearInterval(generateBall);
      clearInterval(gameAninmation);
    }
    if (gameStatus == "PAUSE") {
      clearInterval(generateBall);
      clearInterval(gameAninmation);
    }
  }, timeBallAnimation);
}
//Hàm tạo ra bong bóng nhanh dần theo thời gian
function createMultiBall() {
  generateBall = setTimeout(() => {
    if (timeGenerateBall > 1000) {
      timeGenerateBall -= 30;
      if (timeBallAnimation > 0) {
        timeBallAnimation -= 1;
      }
    }
    createBall(random(56), random(870), color[random(8)]);
    createMultiBall();
  }, timeGenerateBall);
}
///////////////////////////////////// EVENT FUNCTION /////////////////////////////////

//Bắt sự kiện bàn phím
let btnRankOn = document.querySelector(".ranking-on");
let btnRankOff = document.querySelector(".ranking-off");
let rankTable = document.querySelector(".rank");
btnRankOn.addEventListener("click", function () {
  rankTable.style.display = "block";
  getTopRank();
  btnRankOn.style.display = "none";
  btnRankOff.style.display = "block";
});

btnRankOff.addEventListener("click", function () {
  rankTable.style.display = "none";
  btnRankOn.style.display = "block";
  btnRankOff.style.display = "none";
});

const keyDown = document.addEventListener("keydown", function (e) {
  if (
    gameStatus != "PAUSE" &&
    gameStatus != "GAME_OVER" &&
    localStorage.getItem("id")
  ) {
    let scoreElement = document.querySelector(".score");
    let key = document.querySelector(".key");
    if (e.keyCode >= 65 && e.keyCode <= 90) {
      string += e.key;

      key.innerHTML = ` <p> ${string} </p>`;

      let ball = document.querySelector("." + string);
      if (ball) {
        if (ball.classList.value.includes("boom")) {
          getScoreById();
          gameStatus = "GAME_OVER";

          timeBallAnimation = 100;
          timeGenerateBall = 2000;
          loadAudio(".music-bg", true, 0);

          if (score == localStorage.getItem("score")) {
            over.innerHTML = `<p>GAME OVER</p>
            <p>High score: ${score}</p>`;
            loadAudio(".high-score", true, 0.5);
            //insertScore();
          } else {
            over.innerHTML = `<p>GAME OVER</p>
            <p>Your score: ${score}</p>`;
          }

          over.style.display = "block";
          if (gameMusic == "ON") {
            loadAudio(".game-over", true, 0.5, null);
            loadAudio(".boom-audio", true, 1, null);
          }

          clearInterval(generateBall);
          clearInterval(gameAninmation);
          score--;
        } else if (ball.classList.value.includes("start")) {
          score++;
        }
        score++;
        if (score > localStorage.getItem("score")) {
          localStorage.setItem("score", score);
        }

        //
        if (gameMusic == "ON") {
          const html = `<source src="./public/audio/hiragana/${string}.mp3" type="audio/mpeg">`;
          loadAudio(".audio", true, 1, html);
        }

        ball.remove();
        scoreElement.innerHTML = `Score: ${score}`;
        string = "";
        setTimeout(() => {
          key.innerHTML = ` <p> ${string} </p>`;
        }, 300);
      }
    }
    //xoa 1 ki tu
    if (e.keyCode == 8) {
      string = string.substring(0, string.length - 1);
      key.innerHTML = ` <p> ${string} </p>`;
    }
    //phim cach xoa ball
    if (e.keyCode == 32) {
      let ball = document.querySelector(".ball");
      let arrow = document.querySelector(".arrow-img");
      if (arrow && ball) {
        if (gameMusic == "ON") {
          const html = `<source src="./public/audio/arrow.mp3" type="audio/mpeg">`;
          loadAudio(".audio", true, 0.1, html);
        }

        arrow.remove();
        if (ball) {
          ball.remove();
        }
      }
    }
    if (string.length > 6) {
      string = "";
      key.innerHTML = ` <p> ${string} </p>`;
    }
  }
});

//Trò chơi mới
btnNewGame.addEventListener("click", function () {
  if (localStorage.getItem("id")) {
    score = 0;
    //over.style.display = "none";
    let ball = document.querySelectorAll(".ball");
    if (checkGameOver() || gameStatus == "GAME_OVER") {
      over.style.display = "none";
      if (ball) {
        ball.forEach((ball) => {
          ball.remove();
        });
      }
      gameover = false;
    }

    runGame();
    //
    btnResume.style.display = "none";
    btnPause.style.display = "block";
  }
});

//Tạm dừng game
btnPause.addEventListener("click", function () {
  if (localStorage.getItem("id")) {
    if (gameStatus != "GAME_OVER") {
      gameStatus = "PAUSE";
      btnPause.style.display = "none";
      btnResume.style.display = "block";
    }
  }
});

//Tiếp tục game
btnResume.addEventListener("click", function () {
  if (localStorage.getItem("id")) {
    if (gameStatus != "GAME_OVER") {
      gameStatus = "RESUME";
      runGame();
      btnResume.style.display = "none";
      btnPause.style.display = "block";
    }
  }
});
//////////////////////////////////////////COMMON FUNCTION ////////////////////////////////////////////
//Hàm mở file mp3
const loadAudio = (element, autoplay, volume, html) => {
  let audio = document.querySelector(element);
  if (html) {
    audio.innerHTML = `${html}`;
  }
  audio.autoplay = autoplay;
  audio.volume = volume;
  audio.load();
};

//Hàm tắt loa
btnOn.addEventListener("click", function () {
  btnOn.style.display = "none";
  btnOff.style.display = "block";
  loadAudio(".music-bg", true, 0);
  gameMusic = "OFF";
});
//Hàm mở loa
btnOff.addEventListener("click", function () {
  btnOn.style.display = "block";
  btnOff.style.display = "none";
  loadAudio(".music-bg", true, 0.2, null);
  gameMusic = "ON";
});

//Hàm kiểm tra gameover
const checkGameOver = () => {
  let ball = document.querySelector(".ball");
  if (ball) {
    let ballPosition = ball.getBoundingClientRect();
    if (ballPosition.top > 680) {
      if (ball.classList.value.includes("boom")) {
        ball.remove();
        return false;
      } else {
        gameStatus = "GAME_OVER";
        getScoreById();
        if (score == localStorage.getItem("score")) {
          over.innerHTML = `<p>GAME OVER</p>
          <p>High score: ${score}</p>`;
          loadAudio(".high-score", true, 0.5);
          //score = 0;
          //insertScore();
        } else {
          over.innerHTML = `<p>GAME OVER</p>
          <p>Your score: ${score}</p>`;
        }
        string = "";
        let key = document.querySelector(".key");

        key.innerHTML = ` <p> ${string} </p>`;
        over.style.display = "block";
        return true;
      }
    } else {
      return false;
    }
  }
};

//Hàm random
function random(max) {
  return Math.floor(Math.random() * max);
}
////////////////////////////////////////////////////////////////////////////////////////////////////

async function insertScore() {
  let name = localStorage.getItem("name");
  let score = localStorage.getItem("score");
  // Sent Data to Sever
  const data = {
    name: name,
    score: score,
  };
  const url = "./newscore.php";
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      Accept: "application/json;charset=utf-8",
    },
    body: JSON.stringify(data),
  });
  // Get Result
  const result = await response.json();
  if (result) {
    // Get profile
    localStorage.setItem("id", result[0].id);
    localStorage.setItem("name", result[0].name);
  }
}

//
async function getScoreById() {
  console.log("Goi ham getScoreById()");
  let id = localStorage.getItem("id");
  // Sent Data to Sever
  const data = {
    id: id,
  };
  const url = "./getscorebyid.php";
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      Accept: "application/json;charset=utf-8",
    },
    body: JSON.stringify(data),
  });
  // Get Result
  const result = await response.json();

  if (result) {
    if (localStorage.getItem("score") > result[0].score) {
      console.log("Vo");
      updateScore();
    }
  }
}

async function updateScore() {
  console.log("vo update");
  // Sent Data to Sever
  let score = localStorage.getItem("score");
  let id = localStorage.getItem("id");
  const data = {
    id: id,
    score: score,
  };
  const url = "./updatescore.php";
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      Accept: "application/json;charset=utf-8",
    },
    body: JSON.stringify(data),
  });
  // Get Result
  const result = await response.json();
  console.log("RS UPDATE: ", result);
}

async function getTopRank() {
  // Sent Data to Sever

  const url = "./gettoprank.php";
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      Accept: "application/json;charset=utf-8",
    },
  });
  // Get Result
  const result = await response.json();
  if (result) {
    let num = 1;
    let tbody = document.querySelector(".tbody");
    tbody.innerHTML = "";
    result.forEach(function (item) {
      tbody.innerHTML += `<tr>
      <th>${num++}</th>
      <th>
          ${item.name}
      </th>
      <th>
          ${item.score}
      </th>

  </tr>`;
    });
  }
}
