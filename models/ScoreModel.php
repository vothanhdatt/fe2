<?php

require_once 'BaseModel.php';

class ScoreModel extends BaseModel {

    /**
     * Update score
     * @param $input
     * @return mixed
     */
    public function updateScore($id, $score) {
        //$id = $this->decryptID($input['id']);
        $sql = 'UPDATE `score` SET 
                 score = "' . $score .'"
                WHERE id = ' . $id;
        $score = $this->update($sql);
        return $score;
    }

    /**
     * Insert score
     * @param $input
     * @return mixed
     */
    public function insertScore($name, $score) {
        // SQL
        $sql = "INSERT INTO `score`(`name`, `score`) 
        VALUES ('".$name."','".$score."')";
        $this->insert($sql);
        $sql_select = "SELECT MAX(id) as id, name, score FROM `score`";
        $score = $this->select($sql_select);
        return $score;
    }

    

    /**
     * \
     * @param array $params
     * @return array
     */
    public function getScoreById($id) {
        //Keyword

            $sql = 'SELECT * FROM score WHERE id LIKE '. $id;
            $score = $this->select($sql);
        

        return $score;
    }

     /**
     * \
     * @param array $params
     * @return array
     */
    public function getTopRank() {
        //Keyword

            $sql = 'SELECT * FROM `score` ORDER BY score DESC LIMIT 5 ';
            $topRank = $this->select($sql);
        

        return $topRank;
    }
}   