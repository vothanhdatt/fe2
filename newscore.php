<?php
require_once './models/ScoreModel.php';
$scoreModel = new ScoreModel();
spl_autoload_register(function ($class_name) {
    require '../app/models/' . $class_name . '.php';
});

$input = json_decode(file_get_contents('php://input'), true);


$name = $input['name'];
$score = $input['score'];

$data = $scoreModel->insertScore($name, $score);
echo json_encode($data);
?>